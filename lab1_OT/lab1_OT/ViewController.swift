//
//  ViewController.swift
//  lab1_OT
//
//  Created by student on 03.03.17.
//  Copyright © 2017 KI-13-5. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let firstPage: Web = Web(address: "http://asdf", name: "asdf", volume: 10, htmlText: "hello")
        let secondPage: Web = Web(address: "http://lkjhg", name: "lkjhg", volume: 20, htmlText: "world!")
        
        print(firstPage.fullDescription())
        print(secondPage.fullDescription())
        
        firstPage.AddCurrentPage(page: firstPage)
        secondPage.AddCurrentPage(page: secondPage)
        
        
        firstPage.PrintCurrentPages()
        secondPage.PrintCurrentPages()
        
        let number:NSNumber = NSNumber.init(value: 3)
        print(number.isInInterval(beginValue: 1, endValue: 4))
        print(number.isInInterval(beginValue: 1, endValue: 2))
    }

}
