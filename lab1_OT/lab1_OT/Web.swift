//
//  WEb.swift
//  lab1_OT
//
//  Created by student on 03.03.17.
//  Copyright © 2017 KI-13-5. All rights reserved.
//

import UIKit

class Web: CustomStringConvertible, FullDescriptionProtocol {
    
    var address:String
    var name: String
    var volume: Int
    var htmlText : String
    var currentPages : Array<Web>

    var description: String { return "Address: \(address) and name \(name) with volume \(volume), html: \(htmlText)" }
    
    init ()
    {
        
        address = ""
        name = ""
        volume = 0
        htmlText = ""
        currentPages = Array()
        
    }
    
    func AddCurrentPage(page: Web)
    {
        currentPages.append(page)
    
    }
    
    func PrintCurrentPages()
    {
        print(currentPages)
    }

    convenience init (address:String, name: String, volume: Int, htmlText: String)
    {
        self.init()
        self.address = address
        self.name = name
        self.volume = volume
        self.htmlText = htmlText
    }
    
    func fullDescription() -> String {
        return "Address: \(address) and name \(name) with volume \(volume), html: \(htmlText)"
    }
    
}


