//
//  FullDescriptionProtocol.swift
//  lab1_OT
//
//  Created by Aleksandr on 10.03.17.
//  Copyright © 2017 KI-13-5. All rights reserved.
//

import Foundation

protocol FullDescriptionProtocol {
    func fullDescription() -> String
}
