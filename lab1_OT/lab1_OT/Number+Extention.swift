//
//  Number+Extention.swift
//  lab1_OT
//
//  Created by student on 03.03.17.
//  Copyright © 2017 KI-13-5. All rights reserved.
//

import Foundation

extension NSNumber {
    func isInInterval(beginValue: Int, endValue: Int) -> Bool {
        return self.intValue >= beginValue && self.intValue <= endValue
    }
}
